<?php
require('s_function.php');
class BaseApi extends Api{

	public function _initialize(){
		$login = $this->is_login();
		if($login){
			$this->mid = $login['uid'];
		}
	}

	public function response($code,$statusTag,$statusMsg,$data){
		header("Content-type: application/json; charset=utf-8");  
		exit (json_encode(array(	'code'=>$code,
									'statusTag'=>$statusTag,
									'statusMsg'=>$statusMsg,
									'data'=>$data
									)));
	}
	
	public function verifyError(){
		header("Content-type: application/json; charset=utf-8");
		$this->response('0002','','verify failed');
	}
	
	public function verifyOauth($oauth_token,$oauth_token_secret,$type="location"){
		$verifycode = array();
		$verifycode['oauth_token'] = $oauth_token;
		$verifycode['oauth_token_secret'] = $oauth_token_secret;
		$verifycode['type'] = $type;
		$login = D('Login')->where($verifycode)->field('uid,oauth_token,oauth_token_secret')->find();
		return $login;
	}

	public function is_login(){
		$this->login = $this->verifyOauth($this->data['oauth_token'],$this->data['oauth_token_secret']);
		return $this->login;
	}
}