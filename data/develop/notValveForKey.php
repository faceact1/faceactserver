<?php
return array (
  'PUBLIC_GET_USERINFO_FAIL' => '?app=widget&mod=FeedList&act=loadNew',
  'LOGIN_FAILED' => '?app=api&mod=Oauth&act=authorize',
  'LOGIN_SUCCESS' => '?app=api&mod=Oauth&act=authorize',
  'REGISTER_SUCCESS' => '?app=api&mod=Oauth&act=register',
  'VERIFY_CODE_SENT' => '?app=api&mod=Oauth&act=request_phone_code',
  'MISSED_TYPE_PARAMETER' => '?app=api&mod=Oauth&act=request_phone_code',
  'VERIFY_CODE_WRONG' => '?app=api&mod=Oauth&act=phone_verify',
  'VERIFY_CODE_OK' => '?app=api&mod=Oauth&act=phone_verify',
  'EMAIL_SENT' => '?app=api&mod=Oauth&act=reset_password',
  'THE LINK FOR RESET THE PASSWORD IS OUT OF DATE,PLEASE TRY AGAIN THE APP' => '?app=api&mod=Oauth&act=do_reset_password',
  'YOUR PASSWORD HAS BEEN RESET. YOUR NEW PAASWORD IS ' => '?app=api&mod=Oauth&act=do_reset_password',
  'PASSWORD NOT CORRECT' => '?app=api&mod=Oauth&act=set_password',
  'PASSWORD CHANGED' => '?app=api&mod=Oauth&act=set_password',
  'OAUTH VERIFY FAILED' => '?app=api&mod=Oauth&act=logout',
  'LOGOUT SUCCESS' => '?app=api&mod=Oauth&act=logout',
  'PUBLIC_WEIBA' => '?app=public&mod=Index&act=index',
);
?>